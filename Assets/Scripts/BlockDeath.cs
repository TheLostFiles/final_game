﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockDeath : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "PuzzleBlockBlue" || other.gameObject.tag == "PuzzleBlockRed" || other.gameObject.tag == "PuzzleBlockGreen") // checks for the puzzle blocks
        {
            Destroy(other.gameObject); // destroys
        }
    }
}
