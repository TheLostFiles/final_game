﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [NonSerialized] public bool P1Check = false;
    [NonSerialized] public bool P2Check = false;

    void Awake()
    {
        if (instance == null) // checks if the instance is null
        {
            instance = this; // Store This instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one - this new object must die
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }
    }
    void Update()
    {
        if (P1Check && P2Check) // checks if both of these are true
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // jumps to the next scene
            P1Check = false; // sets to false
            P2Check = false; // sets to false
        }
    }
}
