﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPurple : MonoBehaviour
{
    SpriteRenderer sr;

    public GameObject blueBlock;
    private GameObject blockLife;

    private Color32 originalColor;
    private float orgR;
    private float orgG;
    private float orgB;
    private float orgA;

    public bool inUse;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>(); // grabs the sprite renderer
        originalColor = sr.color; // sets the original color
        orgR = originalColor.r / 255.0f; // makes sure that it doesn't pu them all at the setting of 1
        orgG = originalColor.g / 255.0f; // makes sure that it doesn't pu them all at the setting of 1
        orgB = originalColor.b / 255.0f; // makes sure that it doesn't pu them all at the setting of 1
        orgA = originalColor.a / 255.0f; // makes sure that it doesn't pu them all at the setting of 1
    }
    void Update()
    {
        if (blockLife == null) // checks if null
        {
            inUse = false; // sets to false
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player1") // checks for player
        {
            sr.color = Color.green; // makes green
            if (inUse == false) // checks if false
            {
                blockLife = Instantiate(blueBlock); // sets a game object to the object that then Instantiates
                inUse = true; // sets to true
            }
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player1") // checks for player
        {
            sr.color = new Color(orgR, orgG, orgB, orgA); // sets back to the original color

        }
    }
}