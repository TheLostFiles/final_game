﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    private float Movement;

    public float Speed;
    public float Jumpforce;

    public int Jumps;
    [NonSerialized] public int extraJumps;
    
    public bool isGroundedP1 = true;
    public bool isGroundedP2 = true;

    public P1 p1;
    public P2 p2;

    public void Move(float Speed, Rigidbody2D rb2d, Transform tf)
    {
        Movement = Input.GetAxis("Horizontal") * Speed; // movement 
        rb2d.velocity = new Vector2(Movement, rb2d.velocity.y); // movement still
    }
    public void Jump(float Jumpforce, Rigidbody2D rb2d, Transform tf)
    {
        if (Input.GetButtonDown("Jump") && extraJumps > 0) // checks for pressing space and in you hva enough jumps
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, Jumpforce); // makes the player jump
            isGroundedP1 = false; // isGrounded = false
            isGroundedP2 = false;
            extraJumps--; // takes away a jump
        }

        if (isGroundedP1 || isGroundedP2) // checks if they are grounded
        {
            extraJumps = Jumps; // sets extra jumps o jumps
        }
    }
    public void Swap() 
    {
        if (Input.GetKeyDown("f")) // checkd for the press of f
        {
            p1.enabled = !p1.enabled; // swaps the enabledness
            p2.enabled = !p2.enabled;// swaps the enabledness

        }
    }
    public void kBye()
    {
        if (Input.GetKeyDown("r")) // checks for r
        {
            Scene currentScene = SceneManager.GetActiveScene(); // gets the active scene
            SceneManager.LoadScene(currentScene.name, LoadSceneMode.Single); // reloads the scene
        }
    }
}
