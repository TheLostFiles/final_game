﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2 : Controller
{
    private Rigidbody2D rb2d;
    private Transform tf;

    void Start()
    {
        tf = GetComponent<Transform>();
        rb2d = GetComponent<Rigidbody2D>();
        p2.enabled = false;
    }
    void Update()
    {
        Move(Speed, rb2d, tf);
        Jump(Jumpforce, rb2d, tf);
        Swap();
        kBye();
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Flooring" || other.gameObject.tag == "Player1" || other.gameObject.tag == "PuzzleBlockRed" || other.gameObject.tag == "PuzzleBlockBlue" || other.gameObject.tag == "PuzzleBlockGreen" || other.gameObject.tag == "End") // checks floor
        {
            isGroundedP2 = true;
        }
    }
    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Flooring" || other.gameObject.tag == "Player1" || other.gameObject.tag == "PuzzleBlockRed" || other.gameObject.tag == "PuzzleBlockBlue" || other.gameObject.tag == "PuzzleBlockGreen" || other.gameObject.tag == "End")  // checks floor
        {
            isGroundedP2 = false;
        }
    }

}
