﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1 : Controller
{
    private Rigidbody2D rb2d;
    private Transform tf;

    void Start()
    {
        tf = GetComponent<Transform>(); 
        rb2d = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        Move(Speed, rb2d, tf); // mover
        Jump(Jumpforce, rb2d, tf); // jumper
        Swap(); // swapper
        kBye();
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Flooring" || other.gameObject.tag == "Player2" || other.gameObject.tag == "PuzzleBlockRed" || other.gameObject.tag == "PuzzleBlockBlue" || other.gameObject.tag == "PuzzleBlockGreen" || other.gameObject.tag == "End") // checks floor
        {
            isGroundedP1 = true; // sets to true
        }
    }
    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Flooring" || other.gameObject.tag == "Player2" || other.gameObject.tag == "PuzzleBlockRed" || other.gameObject.tag == "PuzzleBlockBlue" || other.gameObject.tag == "PuzzleBlockGreen" || other.gameObject.tag == "End")  // checks floor
        {
            isGroundedP1 = false; // sets to true
        }
    }
    
}
