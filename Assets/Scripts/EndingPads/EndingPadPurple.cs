﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingPadPurple : MonoBehaviour
{
    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player1") // checks for player
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.green; // changes to green
            GameManager.instance.P1Check = true;// switches to true
        }
    }
    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player1") // checks for player
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.grey; // changes to grey
            GameManager.instance.P1Check = false;// switches to false
        }
    }
}
