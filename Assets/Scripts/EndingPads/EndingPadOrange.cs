﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingPadOrange : MonoBehaviour
{
    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player2") // checks for player 2
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.green; // changes to green
            GameManager.instance.P2Check = true; // switches to true
        }
    }
    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player2") // checks for player 2
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.grey;// changes to grey
            GameManager.instance.P2Check = false; // switches to false
        }
    }
}
