﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void playButton()
    {
        SceneManager.LoadScene(1); // loads scene 1
    }
    public void quitButton()
    {
        SceneManager.LoadScene(6); // loads scene 5
    }
    public void quitButtonReal()
    {
        Application.Quit(); // quits
    }
}
